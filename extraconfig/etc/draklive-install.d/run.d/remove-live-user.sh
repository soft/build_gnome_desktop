#!/bin/bash

# remove live user configurations
rm -f /var/lib/AccountsService/icons/live
rm -f /var/lib/AccountsService/users/live

# remove live schemas
rm -f /usr/share/glib-2.0/schemas/org.gnome.desktop.screensaver.gschema.override
rm -f /usr/share/glib-2.0/schemas/org.gnome.desktop.lockdown.gschema.override

# remove live favarite apps from gnome-shell
sed -i '/favorite-apps/d' /usr/share/glib-2.0/schemas/org.gnome.shell.gschema.override

/usr/bin/glib-compile-schemas /usr/share/glib-2.0/schemas/ > /dev/null 2>&1

# remove images from root
rm -f /*.jpg

# drop all AutoLogin lines from gdm custom.conf
sed -i '/AutomaticLogin/d' /etc/X11/gdm/custom.conf

# workarround for http://bugs.rosalinux.ru/show_bug.cgi?id=5128#c4
sed -i '/ExecStartPre=/d' /lib/systemd/system/gdm.service
