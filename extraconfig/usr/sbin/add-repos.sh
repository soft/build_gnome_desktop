#!/bin/sh

ARCH=`uname -m`

if [ "$ARCH" != "x86_64" ]; then
	ARCH="i586"
fi

/usr/sbin/urpmi.addmedia --distrib  --all-media http://abf.rosalinux.ru/downloads/rosa2012.1/repository/$ARCH/
